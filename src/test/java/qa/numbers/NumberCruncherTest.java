package qa.numbers;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class NumberCruncherTest {


    @Test
    public void find_highest_number_in_array_of_one() throws MyException{
        //arrange
        int[] input = {3};
        int expectedResult = 3;
        NumberCruncher cut = new NumberCruncher();

        //act
        int actualResult = cut.findHighestNumber(input);

        //assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void find_highest_number_in_array_of_13_and_4() throws MyException{
        //arrange
        int[] input = {13, 4};
        int expectedResult = 13;
        NumberCruncher cut = new NumberCruncher();

        //act
        int actualResult = cut.findHighestNumber(input);

        //assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void find_highest_number_in_array_of_7_and_13() throws MyException{
        //arrange
        int[] input = {7, 13};
        int expectedResult = 13;
        NumberCruncher cut = new NumberCruncher();

        //act
        int actualResult = cut.findHighestNumber(input);

        //assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void find_highest_number_in_long_array() throws MyException{
        //arrange
        int[] input = {4, 5, -8, 3, 11, -21, 6};
        int expectedResult = 11;
        NumberCruncher cut = new NumberCruncher();

        //act
        int actualResult = cut.findHighestNumber(input);

        //assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void find_highest_number_in_equal_and_highest_array() throws MyException{
        //arrange
        int[] input = {4, 5, 11, 3, 11, -21, 6};
        int expectedResult = 11;
        NumberCruncher cut = new NumberCruncher();

        //act
        int actualResult = cut.findHighestNumber(input);

        //assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void use_lambdas() {
        //arrange
        int[] input = {};
        NumberCruncher cut = new NumberCruncher();

        //act
        assertThrows(MyException.class,
                () -> cut.findHighestNumber(input));

    }
}
