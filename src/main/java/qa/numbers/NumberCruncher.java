package qa.numbers;

import java.util.Arrays;

public class NumberCruncher {

    public int findHighestNumber(int[] input) throws MyException {
        if(input.length < 1){
            throw new MyException("Array length should be greater than zero");
        }
        else {
        int max = input[0];

        for(int counter = 0; counter < input.length; counter++){
            if(input[counter] > max){
                max = input[counter];
            }
        }
        return max;
        }
    }
}
