package qa.numbers;

public class MyException extends Exception {
    private String message;

    public MyException(String errorMessage) {
        message = errorMessage;
    }

    public String getMessage(){
        return message;
    }
}
